# Repo for cgDNA+ Projects
This is is a collection of different projects which are related to cgDNA+.

You can find more info on cgDNA+ here

https://lcvmwww.epfl.ch/research/cgDNA/

The repo contains:
 - cgDNAplus contains the cgDNAplus model
 - outlier is a project for outlier detection
 - xgb is a classification project based on xgb models
 - network is regression problem based on networks

More details on  the project and the used data can be found in the README of the individual project folder.
