# Repo for cgDNAplus
This code has been developed by Alessandro Patelli in his thesis and is not further developed. For information on installation can be found in the INSTALLATION file. 

In order to use cgDNAplus you can find further info in the main.py file
```
import sys
from classes.cgDNAclass import cgDNA

a = cgDNA('GCGCATATGCGC')
a.MonteCarlo(100)
a.ground_state
a.stiff
a.paramset

b = cgDNA('ACGCATATGCGC')
b.MonteCarlo(100)
b.ground_state
b.stiff

diff = a.stiff - b.stiff
```

