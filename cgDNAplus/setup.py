from setuptools import setup

setup(
    name='cgDNAplus',
    version='0.1.0',
    description='Python package for cgDNA+',
    url='epfl.ch',
    author='Alessandro Patelli',
    author_email='alessandro.patelli@epfl.ch',
    install_requires=['numpy','scipy','matplotlib'],
    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Science/Research',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.6',
    ],
)
