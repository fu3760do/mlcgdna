#!/usr/bin/env python
# coding: utf-8


import sys
sys.path.append('./moduels')
sys.path.append('./classes')
from cgDNAclass import cgDNA

import pandas as pd
import os
import numpy as np
import numpy.linalg
import matplotlib.pyplot as plt
import scipy.linalg as scl
import scipy as sc
import scipy.sparse.linalg as ssl



def calculate_eigs(data):
    cg = cgDNA(data)
    val1, _ = ssl.eigs(cg.stiff,k=1,which='LR')
    val2, _ = ssl.eigs(cg.stiff,k=1,which='SR')
    return val1[0], val2[0]
'''
if __name__ == "__main__":
    path = 'GSE65073_RAW/'
    txt_list = os.listdir(path)
    txt_list = txt_list[1]
    cmax = []; cmin=[];
    for file in txt_list:
        lcmax = []; lcmin = [];
        data = pd.read_csv(os.path.join(path,txt_list), nrows=10, sep='\t', dtype={'Kmer':str,'relKa':float})
        res = p.map(calculate_eigs,data['Kmer'])
    
        [lcmax.append(np.real(res[i][0])) for i in range(len(res))]
        [lcmin.append(np.real(res[i][1])) for i in range(len(res))]
    
        data['max_eig'] = lcmax
        data['min_eig'] = lcmin
    
        c_max = np.corrcoef(data['relKa'],data['max_eig'])[1,0]
        c_min = np.corrcoef(data['relKa'],data['min_eig'])[1,0]
    
        cmax.append(c_max)
        cmin.append(c_min)
    corr_data = pd.DataFrame({'name': txt_list[:1], 'cmax': cmax, 'cmin': cmin})
#corr_data.to_csv('cgDNA_vs_relKa_correlation.csv')
print('done')
print(corr_data)
'''




