#dockerfile for cgDNA and pytorch

FROM python:3.9.6
RUN apt-get update -y
RUN apt-get install -y python3-pip python-dev build-essential
RUN pip install --upgrade pip 
RUN apt-get install -y ghostscript libgs-dev
RUN apt-get install -y libmagickwand-dev imagemagick --fix-missing
RUN apt-get install -y libpng-dev zlib1g-dev libjpeg-dev
RUN apt-get install vim -y
RUN apt-get install tmux -y

COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY cgDNAplus/dist/cgDNAplus-0.1.0-py3-none-any.whl ./
RUN pip install --no-cache-dir cgDNAplus-0.1.0-py3-none-any.whl 
WORKDIR /usr/src/app                                          
