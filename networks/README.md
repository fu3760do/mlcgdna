# Repo for regression based on network

In this project I used neural network in order to predict the relKa values.
The project is motivated by the follwoing paper
[Li et. al. 2017](https://academic.oup.com/nar/article/45/22/12877/4634013?login=false)
and the data used for this project can also be found in this paper.
