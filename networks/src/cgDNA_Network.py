#!/usr/bin/env python
# coding: utf-8
import os
import json
import pandas as pd
import torch as T
import torch.optim as optim
import torch.nn as nn
import argparse
import itertools
from pathlib import Path
from sklearn.metrics import mean_squared_error, r2_score

import preprocessing as prep
import linear_model
import make_folder as mf

parser = argparse.ArgumentParser()
parser.add_argument('-l', default=1, help='pick argument from lr_lst')
parser.add_argument('-e', default='one_hot', help='pick encoding from (one_hot, dna, cg, stiff)')
parser.add_argument('-n', default='None', help='pick name of a file')
args = parser.parse_args()


lr_lst = [5e-2, 1e-2, 5e-3, 1e-3, 5e-4, 1e-4, 5e-5, 1e-5, 5e-6]
hidden_lst = [1, 2, 3]
batch_lst = [16, 32, 64, 128, 256]
para = list(itertools.product(lr_lst, hidden_lst, batch_lst)) # 135
num = mf.num_exp

in_path = Path('..res/encoding/') # PATH(/scratch/fu3760do/res/encoding)
out_path = Path('../res/') # PATH(/scratch/fu3760do/res)

#TODO:
# make all files once such that the get data only does the train and testloader

def get_data(name: str, encode: str, bsize=32):

    encode = encode.split('+')
    filename = name[:-4]
    oh_file = in_path / f"{filename}_one_hot.csv"
    if oh_file.is_file():
        data = pd.read_csv(str(oh_file))
    else:
        data = prep.get_mers(name, mer=[1, 0, 0], save=True)
    if 'dna' in encode:
        dna_file = in_path / f"{filename}_dnashape_scaled.csv"
        if dna_file.is_file():
            dna = pd.read_csv(str(dna_file))
        else:
            dna = prep.get_dnashape(name, save=True)
        data = pd.merge(data, dna, left_index=True, right_index=True)
        data.drop(['target_x'], axis=1, inplace=True)
        data.rename({'target_y': 'target'}, axis='columns', inplace=True)
    elif 'cg' in encode:
        cg_file = in_path / f"{filename}_cg_scaled.csv"
        if cg_file.is_file():
            cg = pd.read_csv(str(cg_file))
        else:
            cg = prep.get_cgdna_gs(name, save=True)
        data = pd.merge(data, cg, left_index=True, right_index=True)
        data.drop(['target_x'], axis=1, inplace=True)
        data.rename({'target_y': 'target'}, axis='columns', inplace=True)
    elif 'stiff' in encode:
        cgs_file = in_path / f"{filename}_cgs_scaled.csv"
        if cgs_file.is_file():
            cgs = pd.read_csv(str(cgs_file))
        else:
            cgs = prep.get_cgstiff(name, save=True)
        data = pd.merge(data, cgs, left_index=True, right_index=True)
        data.drop(['target_x'], axis=1, inplace=True)
        data.rename({'target_y': 'target'}, axis='columns', inplace=True)

    trainloader, testloader = prep.get_dataloader(data, bsize)
    return trainloader, testloader


def train(model, opti, loss_fn, train_loader, test_loader, mdata):
    epochs = mdata['epochs']
    device = 'cpu'
    train_lst = []
    test_lst = []
    for epoch in range(epochs):
        train_loss = test_loss = 0.0
        model.train()
        for batch in train_loader:
            opti.zero_grad()
            inputs, labels = batch
            inputs = inputs.float().to(device)
            output = model(inputs)
            loss = loss_fn(output.float(), labels.unsqueeze(1).float())
            loss.backward()
            opti.step()
            train_loss += loss.data.item()*inputs.size(0)
        train_loss /= len(train_loader.dataset)
        train_lst.append(train_loss)
        
        model.eval()
        outs = []
        labs = []
        for batch in test_loader:
            inputs, labels = batch
            inputs = inputs.float().to(device)
            output = model(inputs)
            if epoch == epochs-1:
                outs.append(output.data)
                labs.append(labels.data)
            loss = loss_fn(output, labels.unsqueeze(1))
            test_loss += loss.data.item()*inputs.size(0)
        test_loss /= len(test_loader.dataset)
        test_lst.append(test_loss)
        
    outs = [outs[i][j].item() for i in range(len(outs)) for j in range(len(outs[i]))]
    labs = [labs[i][j].item() for i in range(len(labs)) for j in range(len(labs[i]))]
    df = pd.DataFrame()
    df['pred'] = outs
    df['truth'] = labs
    df.sort_values(by='truth', inplace=True)
    cor = df.corr().iloc[0, 1]
    mse = mean_squared_error(df['truth'],df['pred'])
    r2 = r2_score(df['truth'],df['pred'])
    ar2 = r2 * ((len(test_loader.dataset)-1)/(len(test_loader.dataset)-int(mdata['nn_dim'])-1))

    mf.make_folder(mdata['name'])
    log_name = mdata['name']+'_'+mdata['encoding']+'_'+str(mdata['learn_rate'])+'_'+str(mdata['hidden_dim'])+'_'+str(mdata['batch_size'])
    nn_name = 'NN'+log_name+'.pkl'
    tmp = {'name': mdata['name'], 'encoding': mdata['encoding'], 'epochs': epochs, 'learn_rate': mdata['learn_rate'],
           'batch_size': mdata['batch_size'], 'hidden_dim': mdata['hidden_dim'], 'optim': 'Adam',
           'nn_name': nn_name, 'train_loss': train_lst, 'test_loss': test_lst, 'corr': cor, 'mse': mse, 'r2': r2, 'ar2': ar2}
    
    with open(out_path / f"experiments_{mdata['name']}_{num}/{log_name}.json", 'w+') as file:
        json.dump(tmp, file)
    T.save(model.state_dict(), open(out_path / f"model_{mdata['name']}_{num}/{nn_name}", 'wb+'))

    df.to_csv(out_path / f"prediction_{mdata['name']}_{num}/{log_name}.csv", index=False)

    return


if __name__ == '__main__':
    name = 'GSM1586782_ScrWT_Exd_14mer_small.txt'  #args.n
    encoding = 'one_hot' #args.e
    trainloader, testloader = get_data(name, encoding)
    num_epochs = 100
    learn_rate = para[int(args.l)-1][0]
    hdim = [trainloader.dataset[0][0].shape[0]]*para[int(args.l)-1][1]
    batch_size = para[int(args.l)-1][2]
    net = linear_model.Net(input_dim=hdim[0], output_dim=1, hidden_dim=hdim).cpu()
    criterion = nn.MSELoss()
    optimizer = optim.Adam(net.parameters(), lr=learn_rate)
    metadata = {'name': name[:-4], 'encoding': encoding, 'epochs': num_epochs, 'learn_rate': learn_rate, 'optim': 'Adam',
                'batch_size': batch_size, 'hidden_dim': para[int(args.l)-1][1], 'nn_dim': hdim[0]}
    train(net, optimizer, criterion, trainloader, testloader, metadata)
