from pathlib import Path
import pandas as pd
import os
from torchvision import transforms
from torch.utils.data import Dataset
import PIL
import matplotlib.pyplot as plt


class CgdnaStiffDataset(Dataset):
    def __init__(self, root, image_dir, csv_file, transform=None):
        self.root = root
        self.image_dir = image_dir
        self.image_files = os.listdir(image_dir)
        self.data = pd.read_csv(csv_file, sep='\t', dtype={'Kmer': str, 'relKa': float}).loc[:, 'relKa']
        self.transform = transform

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        image_name = os.path.join(self.image_dir, self.image_files[index])
        image = PIL.Image.open(image_name)
        label = self.data[index]
        if self.transform:
            image = self.transform(image)
        return image, label


def get_data():
    root = Path('../res/encoding/stiff/')
    image_dir = root/'GSM1586782_ScrWT_Exd_14mer_small'
    csv_file = root / 'GSM1586782_ScrWT_Exd_14mer_small.txt'
    transforms_img = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
    data_set = CgdnaStiffDataset(root, image_dir, csv_file, transforms_img)

    return data_set


def show_image(image, label: float) -> None:
    """
    function for plotting images from dataloader
    call: show_image(*train[0])
    :param image: picture
    :param label: label of picture
    :return: None
    """
    print(f'Label: {label}')
    plt.imshow(image.permute(1, 2, 0))
    plt.show()
