#!/bin/python

import torch.nn as nn
import torch.nn.functional as f

# TODO: Adopt the network to a general data structure


class Convnet(nn.Module):
    def __init__(self):
        super(Convnet, self).__init__()
        self.con1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.con2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16*76*76, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 1)

    def forward(self, x):
        x = self.pool(f.relu(self.con1(x)))
        x = self.pool(f.relu(self.con2(x)))
        x = x.view(-1, 16*76*76)
        x = f.relu(self.fc1(x))
        x = f.relu(self.fc2(x))
        x = self.fc3(x)
        return x
