#!/usr/bin/env python
# coding: utf-8


import json
import os
import sqlite3
from pathlib import Path

path = Path('../res_hpc/experiment_2')
db_path = Path('../res_hpc/')


def drop_table(name):
    conn = sqlite3.connect(os.path.join(db_path,'cg_nn_experiments.db'))
    c = conn.cursor()
    c.execute(f'''DROP TABLE {name}''')
    conn.commit()
    conn.close()


def create_table(name):
    conn = sqlite3.connect(os.path.join(db_path,'cg_nn_experiments.db'))
    c = conn.cursor()
    c.execute(f'''CREATE TABLE IF NOT EXISTS {name}
                (name TEXT,
                 encoding TEXT,
                 epochs REAL,
                 lrate REAL,
                 batch_size INTEGER,
                 hidden_dim INTEGER,
                 optim TEXT,
                 nn_name TEXT,
                 train_loss BLOB,
                 test_loss BLOB,
                 corr REAL,
                 filename TEXT,
                 mse REAL,
                 r2 REAL,
                 ar2 REAL
                )
             ''')
    conn.commit()
    conn.close()
    return


def content_in_db(name):
    create_table(name)
    conn = sqlite3.connect(os.path.join(db_path,'cg_nn_experiments.db'))
    c = conn.cursor()
    folder = path / f'{name}'
    files = os.listdir(folder)
    for file in files:
        with open(os.path.join(folder,file),'r') as f:
            try:
                data = json.load(f)
                c.execute(f'''SELECT nn_name FROM {name}''')
                dbfiles = [item[0] for item in c.fetchall()]
                if data['nn_name'] not in dbfiles:
                    c.execute(f'''INSERT INTO {name} VALUES
                              (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)''', \
                              (data['name'] + str(data['epochs']) + str(data['learn_rate']) + str(data['batch_size']) + str(data['hidden_dim']), \
                               data['encoding'], data['epochs'], data['learn_rate'], data['batch_size'],data['hidden_dim'], \
                               data['optim'], data['nn_name'], json.dumps(data['train_loss']), json.dumps(data['test_loss']), \
                               data['corr'], file, data['mse'], data['r2'], data['ar2']))
            except:
                print(file)

    conn.commit()
    conn.close()
    return


if __name__=='__main__':
    experiments = os.listdir(path)
    for item in experiments:
        print(item)
        content_in_db(item)
