#!/bin/bash
#coding: uft-8


res_folder=/home/jate1/Dokumente/Projekte/mlcgdna/networks/res_hpc

# get data from fu curta
scp fu3760do@curta.zedat.fu-berlin.de:/scratch/fu3760do/res/$1.tar.gz $res_folder

# unpack data into existing folders
tar -xzf $res_folder/$1.tar.gz --directory /home/jate1/Dokumente/Projekte/mlcgdna/networks/res_hpc/experiments_1/

# include data into database
#python create_db.py


