from pathlib import Path
import os

import preprocessing as prep

# define global variables
num_exp = str(2)
out_path = Path('../res')


def make_folder(name: str) -> None:
    if not os.path.exists(out_path):
        os.mkdir(out_path)
    folder_exp = out_path / f'experiments_{name}_{num_exp}'
    if not folder_exp.is_dir():
        os.mkdir(folder_exp)
    folder_pred = out_path / f'prediction_{name}_{num_exp}'
    if not folder_pred.is_dir():
        os.mkdir(folder_pred)
    folder_model = out_path / f'model_{name}_{num_exp}'
    if not folder_model.is_dir():
        os.mkdir(folder_model)

    return


if __name__ == '__main__':
    make_folder(args.n)
    prep.get_all_encodings(args.n)
