#!/bin/python

import pandas as pd
import numpy as np
import os
import pickle
from torch.utils.data import DataLoader
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

import sys
sys.path.append('../../cgDNAplus')
from classes.cgDNAclass import cgDNA


#TODO: - implement cgDNA encoding [done]
#      - implement dnshape encoding [done] 
#      - refactor one hot encoding []

#TODO: 
#   - check 2 and 3 mer encoding

in_path = '../data/files/'
out_path = '../res/encoding/'


def get_mers(name, mer, save):
    #df = pd.read_csv(f'../data/{name}.txt',dtype={'Kmer':str,'relKa':float})
    df = pd.read_csv(f'{in_path}{name}', sep='\t', dtype={'Kmer':str,'relKa':float})
    res = pd.DataFrame()
    hot_mer = []
    for i in range(df.shape[0]):
        two_mer, three_mer = [],[]
        iKmer = df.loc[i,'Kmer']
        hot_mer.append(list(iKmer))
        # check if encoding it correct!
        if mer[0]:
            two_mer = [hot_mer[i]+hot_mer[i+1] for i in range(len(hot_mer)-1)]
        if mer[1]:
            three_mer = [hot_mer[i]+hot_mer[i+1]+hot_mer[i+2]\
                         for i in range(len(hot_mer)-2)]
    # does not make sense to me at the moment!
    res = pd.DataFrame(hot_mer)
    # concat the dataframes
    res = pd.get_dummies(res,dtype='float')
    res['target'] = df['relKa']
    if save:
        res.to_csv(f'{out_path}{name[:-4]}_one_hot.csv', index=False)
    return res


def DNAshape(seq,):
    #load query table
    table_file = '../data/DNA_shape_full_sym_table_updated.csv'
    query_table = pd.read_csv(table_file,index_col='Pentamer')#[0:-3]

    N_seq = len(seq)
    n_features = 13 #columns=['MGW',ProT','Roll','HelT','Stretch','Tilt','Buckle','Shear','Opening','Rise','Shift','Stagger','Slide'])

    #Init feature array
    array_zero = np.empty((N_seq,n_features))
    array_zero[:] = np.NaN
    seq_char = list(seq)
    shape_table = np.zeros((n_features, N_seq))
    # Indices of intra coord + first inter coord in pentamer table
    main_indices = [0,1,2,4,6,7,9,10,11,12,14,16,17]
    second_indices =  [3,5,8,13,15,18] # Indices second inter coord in pentamer table
    intra_ind = [0,1,4,7,6,8,11] #indices of intra coord in feature table
    inter_ind =  [2,3,5,9,10,12] #indices of inter coord in feature table

    #Build feature array
    for i in range(0,N_seq - 4):

        current_pent = seq[i:i+5]
        current_feat = query_table.loc[current_pent]
        feat_block = np.zeros((n_features, N_seq))

        feat_block[:,i] = current_feat[main_indices]
        feat_block[inter_ind,i+1] = current_feat[second_indices]
        np.set_printoptions(formatter={'float': '{: 0.2f}'.format})
        shape_table = shape_table + feat_block

    #Averaging of pentamer block contributions for inter coordinates
    for i in inter_ind:
        shape_table[i,1:N_seq-4] = shape_table[i,1:N_seq-4]/2

    # Replace 4 last (for intras) or 3 last (for inters) values with NaN (they correpond to missing values at both ends of sequence)
    shape_table[intra_ind,-4:] = np.nan
    shape_table[inter_ind,-3:] = np.nan


    # Unravel feature table columns into a single vector, get rid of nan entries
    shape_vector_long = shape_table.flatten('F')
    shape_vector = np.delete(shape_vector_long , np.where(np.isnan(shape_vector_long) ))
    return shape_vector


def get_dnashape(name,save):
    df = pd.read_csv(f'{in_path}{name}',sep='\t',dtype={'Kmer':str,'relKa':float})
    features = []
    for i in range(df.shape[0]):
        features.append(DNAshape(df.loc[i,'Kmer']))

    res = pd.DataFrame(features)

    # Apply Standard Scaler to df
    scaler = StandardScaler()
    res = pd.DataFrame(scaler.fit_transform(res))

    res['target'] = df['relKa']
    if save:
        res.to_csv(f'{out_path}{name[:-4]}_dnashape_scaled.csv',index=False)
    return res


def get_cgdna_gs(name,save):
    df = pd.read_csv(f'{in_path}{name}',sep='\t',dtype={'Kmer':str,'relKa':float})
    features = []
    for i in range(df.shape[0]):
        features.append(cgDNA(df.loc[i,'Kmer']).ground_state)
    res = pd.DataFrame(features)

    # Apply Standard Scaler to df
    scaler = StandardScaler()
    res = pd.DataFrame(scaler.fit_transform(res))
    
    res['target'] = df['relKa']
    if save:
        res.to_csv(f'{out_path}{name[:-4]}_cg_scaled.csv',index=False)
    return res


def get_cgdna_stiff(name,save):
    df = pd.read_csv(f'{in_path}{name}',sep='\t',dtype={'Kmer':str,'relKa':float})
    stiff_lst = []
    for ix in df.index:
        stiff_lst.append([cgDNA(df.loc[ix,'Kmer']).stiff.todense(),df.loc[ix,'relKa']])

    if save:
        with open(f'{out_path}{name[:-4]}_cgdna_stiff.pkl','wb') as f:
            pickle.dump(stiff_lst,f)
    return stiff_lst


def get_cgstiff(name, save):
    df = pd.read_csv(f'{in_path}{name}', sep='\t', dtype={'Kmer': str, 'relKa': float})
    features = []
    for ix in df.index:
        gs = cgDNA(df.loc[ix, 'Kmer']).ground_state.tolist()
        stiff_diag = cgDNA(df.loc[ix, 'Kmer']).stiff.todense().diagonal().tolist()[0]
        lst = gs + stiff_diag
        features.append(lst)

    res = pd.DataFrame(features)

    # Apply Standard Scaler to df
    scaler = StandardScaler()
    res = pd.DataFrame(scaler.fit_transform(res))

    res['target'] = df['relKa']
    if save:
        res.to_csv(f'{out_path}{name[:-4]}_cgs_scaled.csv', index=False)
    return res


def get_all_encodings(name):
    if not os.path.isfile(os.path.join(out_path, f'{name}_one_hot.csv')):
        get_mers(name,[0,0], True)
    if not os.path.isfile(os.path.join(out_path, f'{name}_dnashape_scaled.csv')):
        get_dnashape(name, True)
    if not os.path.isfile(os.path.join(out_path, f'{name}_cgdna_gs_scaled.csv')):
        get_cgdna_gs(name, True)
    if not os.path.isfile(os.path.join(out_path, f'{name}_cgs_scaled.csv')):
        get_cgstiff(name, True)

    return


def create_dataloader(df,labels,batch_size):
    data = []
    for i in df.index:
        data.append([df.loc[i,:].values,labels[i]])

    dataloader = DataLoader(data,batch_size=batch_size)

    return dataloader


def get_dataloader(df, bs=32):
    batch_size = bs
    #sample data
    df = df.sample(frac=1, random_state=42)
    # get labels
    labels = df['target']
    # drop labels from df
    df.drop(['target'], axis=1, inplace=True)

    df_train, df_test, y_train, y_test = train_test_split(df, labels, test_size=0.33, random_state=42)

    testloader = create_dataloader(df_test, y_test, batch_size)
    trainloader = create_dataloader(df_train, y_train, batch_size)

    return trainloader, testloader


if __name__ == '__main__':
    path = '../data/'
    txt_list = os.listdir(path)
    tmp = get_all_encodings('GSM1586804_AbdB_Exd_16mer2_cut_small.txt')
    #tmp = prep.get_mers('GSM1586782_ScrWT_Exd_14mer',[0,0],True)
    #data = data.loc[:100,:]
    #data.to_csv('../data/GSM1586782_ScrWT_Exd_14mer_small.txt',index=False)
    #df = get_mers(data,[1,0,0])
    #df.to_csv('../data/GSM1586782_ScrWT_Exd_14mer_one_hot.txt', index = False)
