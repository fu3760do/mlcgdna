import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset

import cgStiffDataset as Cdata
import conv_model as cmodel


def get_train_test():
    dataset = Cdata.get_data()
    trainset, testset = torch.utils.data.random_split(dataset, [75, 26])
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=4, shuffle=True, num_workers=2)
    testloader = torch.utils.data.DataLoader(testset, batch_size=4, shuffle=True, num_workers=2)

    return trainloader, testloader


def train(net, optimizer, criterion, trainloader):
    for epoch in range(2):
        running_loss = 0.0
        for i, data in enumerate(trainloader, 0):
            inputs, labels = data
            optimizer.zero_grad()
            outputs = net(inputs)
            loss = criterion(outputs.reshape(-1), labels.float())
            loss.backward()
            optimizer.step()

            running_loss += loss.item()
            if i % 2000 == 1999:
                print('[%d, %5d] loss %.3f' % (epoch + 1, i + 1, running_loss / 2000))
                running_loss = 0.0
    print('Training finished')


def main():
    net = cmodel.Convnet()
    criterion = nn.MSELoss()
    optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)
    data_train, data_test = get_train_test()
    train(net, optimizer, criterion, data_train)


if __name__ == '__main__':
    main()
