#!/usr/bin/env python
# coding: utf-8
import sqlite3
import ast
import pandas as pd
import pandas_bokeh

from bokeh.plotting import figure, show
from bokeh.io import output_notebook
output_notebook()

conn = sqlite3.connect('../res_hpc/cg_nn_experiments.db')


def plot_experiments(encoding):
    conn = sqlite3.connect('../res_hpc/cg_nn_experiments.db')
    c = conn.cursor()
    c.execute(f'''SELECT * FROM nn_experiments WHERE encoding = '{encoding}' ''')
    data = c.fetchall()
    name = [cols[0] for cols in c.description]
    tmp = {}
    for d in data:
        for i, elm in enumerate(name):
            if type(d[i]) is str:
                if d[i][0] == '[':
                    tmp[elm] = ast.literal_eval(d[i])
                else: 
                    tmp[elm] = d[i]
            else:
                tmp[elm] = d[i]
        get_plots(tmp)
    conn.close()
    

def get_plots(data):
    p = figure(title=f"{data['filename']} corr:{data['corr']}", x_axis_label='Epochs', y_axis_label='Loss', plot_width=800, plot_height=800)
    p.line(list(range(1, len(data['train_loss'])+1)), data['train_loss'], color='blue', legend_label='Train')
    p.line(list(range(1, len(data['test_loss'])+1)), data['test_loss'], color='red', legend_label='Test')
    p.legend.location = "top_left"
    show(p)


def get_data(name):
    df = pd.read_sql(f"SELECT name,lrate,batch_size,hidden_dim,corr,mse,r2,ar2 FROM {name} WHERE encoding = 'one_hot' ", conn)
    df.rename(columns={"corr":"corr_one_hot","mse":"mse_one_hot","r2":"r2_one_hot","ar2":"ar2_one_hot"}, inplace=True)
    encoding = ['dna','cg','stiff']
    for elm in encoding:
        tmp = pd.read_sql(f'''SELECT name, corr, mse, r2, ar2 FROM {name} WHERE encoding = '{elm}' ''', conn)
        tmp.rename(columns={"corr":f"corr_{elm}","mse":f"mse_{elm}","r2":f"r2_{elm}","ar2":f"ar2_{elm}"}, inplace=True)
        df = pd.merge(df,tmp,on='name')
    return df


def vis_experiment(data,name,metric):
    print(name+' '+metric)
    if metric == 'corr':
        to_drop = ['lrate', 'batch_size','hidden_dim','mse_one_hot','mse_dna','mse_cg','mse_stiff','r2_one_hot',
                   'r2_dna','r2_cg','r2_stiff','ar2_one_hot','ar2_dna','ar2_cg','ar2_stiff']
    elif metric == 'mse':
        to_drop = ['lrate','batch_size','hidden_dim','corr_one_hot','corr_dna','corr_cg','corr_stiff','r2_one_hot',
                   'r2_dna','r2_cg','r2_stiff','ar2_one_hot','ar2_dna','ar2_cg','ar2_stiff']
    elif metric == 'r2':
        to_drop = ['lrate', 'batch_size', 'hidden_dim', 'corr_one_hot', 'corr_dna', 'corr_cg', 'corr_stiff',
                   'mse_one_hot', 'mse_dna', 'mse_cg', 'mse_stiff', 'ar2_one_hot', 'ar2_dna', 'ar2_cg', 'ar2_stiff']
    elif metric == 'ar2':
        to_drop = ['lrate', 'batch_size', 'hidden_dim', 'corr_one_hot', 'corr_dna', 'corr_cg', 'corr_stiff',
               'r2_one_hot', 'r2_dna', 'r2_cg', 'r2_stiff', 'mse_one_hot', 'mse_dna', 'mse_cg', 'mse_stiff']
    dfp = data.drop(to_drop,axis=1)
    dfp.plot_bokeh.point(figsize=(1200,800),
        x="name",
        size=5,
        colormap=['#440154', '#30678D', '#35B778', '#FDE724'],
        title=f"Plot of Network experiments for {name}",
        marker="x")
    #print(data.loc[dfp['corr_one_hot'].idxmax(),:])
    print('One_hot:',dfp[f'{metric}_one_hot'].max())
    print('DNA:',dfp[f'{metric}_dna'].max())
    print('CG:',dfp[f'{metric}_cg'].max())
    print('CG_stiff:',dfp[f'{metric}_stiff'].max())
    return [dfp[f'{metric}_one_hot'].max(),dfp[f'{metric}_dna'].max(),dfp[f'{metric}_cg'].max(),dfp[f'{metric}_stiff'].max()]


def vis_summary(data,name):
    data.plot_bokeh.point(figsize=(1200,800),
        x="name",
        size=5,
        colormap=['#440154', '#30678D', '#35B778', '#FDE724'],
        title=f"Plot of Network experiments {name}",
        marker="x")
    #print(data.loc[dfp['corr_one_hot'].idxmax(),:])

