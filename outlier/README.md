# Repo for outlier detection
In this project I tried to detect binding sites based on cgDNAplus features. 
The data I used is heavy tailed and we used a threshold on relKa (0.7) in order to introduce classes. 

The data which was used in this project can be found in 
[Li et. al. 2017](https://academic.oup.com/nar/article/45/22/12877/4634013?login=false)
