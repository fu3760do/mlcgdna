#!/bin/python
import pandas as pd
import numpy as np
import scipy.linalg as sl
import matplotlib.pyplot as plt
from joblib import Parallel, delayed
import seaborn as sns
import argparse

import sys
sys.path.append('../../cgDNAplus')
from classes.cgDNAclass import cgDNA

parser = argparse.ArgumentParser(description='Generate pots to compare relKa and KL')
parser.add_argument('-n', help='name of a file to process')
args = parser.parse_args()


def kl_sym(mu1, cv1, mu2, cv2):
    N = len(mu1)
    mu12 = np.subtract(mu1, mu2)
    d = (np.trace(sl.solve(cv1, cv2)) + np.trace(sl.solve(cv2, cv1)) - 2 * N + np.matmul(
        np.matmul(mu12.T, np.add(sl.inv(cv1), sl.inv(cv2))), mu12)) / 4
    per_dof = d / N
    return per_dof


def kl(cg0):
    def cal_kl(ix):
        cg = cgDNA(data.loc[ix, 'Kmer'])
        return kl_sym(cg0.ground_state, cg0.stiff.todense(), cg.ground_state, cg.stiff.todense())

    return cal_kl


def append_kl(df):
    cg0 = cgDNA(df.loc[0, 'Kmer'])
    res = kl(cg0)
    answer = Parallel(n_jobs=2)(delayed(res)(i) for i in range(df.shape[0]))
    df['kl'] = answer

    return df


def plotting(df,name):
    name = name[:-4]
    plt.figure(figsize=(10, 10))
    sns.scatterplot(x='relKa', y='kl', data=df)
    corr = df.corr().iloc[0, 1]
    plt.title(f'{name} corr: {corr: .4f}')
    df.to_csv(f'../res/kl_data/files/{name}_kl.csv', index=False)
    plt.savefig(f'../res/kl_data/plots/{name}_kl.png')


def main(data,name):
    tmp = append_kl(data)
    plotting(tmp,name)


if __name__ == '__main__':
    name = str(args.n).strip()
    data = pd.read_csv(f'../data/{name}', sep='\t', dtype={'Kmer': str, 'relKa': float})
    main(data,name)
