import torch
import torch.nn as nn

class autoencoder(nn.Module):
    def __init__(self,data_size):
        super(autoencoder,self).__init__()
        self.encoder = nn.Sequential(
            nn.Linear(data_size,128),
            nn.ReLU(True),
            nn.Linear(128, 64),
            nn.ReLU(True),
            nn.Linear(64,32),
            nn.ReLU(True),
            nn.Linear(32,16),
            nn.ReLU(True),
            nn.Linear(16,8),
            nn.ReLU(True),
            nn.Linear(8,4)
        )
        self.decoder = nn.Sequential(
            nn.Linear(4,8),
            nn.ReLU(True),
            nn.Linear(8,16),
            nn.ReLU(True),
            nn.Linear(16,32),
            nn.ReLU(True),
            nn.Linear(32,64),
            nn.ReLU(True),
            nn.Linear(64,128),
            nn.ReLU(True),
            nn.Linear(128,data_size),
            nn.Tanh())
    
    def forward(self,x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x
