import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import numpy.linalg as nl

from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score, confusion_matrix, precision_recall_fscore_support

from sklearn.svm import OneClassSVM
from scipy.stats import multivariate_normal
from sklearn.ensemble import IsolationForest
from sklearn.covariance import EllipticEnvelope
from sklearn.decomposition import PCA
from sklearn.neighbors import LocalOutlierFactor
from scipy.stats import multivariate_normal
from sklearn.cluster import DBSCAN

def plot_hist(df,save=0):
    '''plot and saves histogramm of relKa'''
    fig = plt.figure(figsize=(10,10))
    df['Label'].hist(bins = 100)
    plt.title('Histogram over relKa',fontsize=16)
    plt.xlabel('relKa')
    plt.plot([0.7,0.7],[0,3500],'r--')

    if save:
        plt.savefig('Hist_GSM1586782_ScrWT_Exd_14mer.jpg')


def get_info(df):
    '''writes out how many datapoints are above a threshold [0,1]'''
    tmp = list(np.linspace(0,1,11))
    for num in tmp:
        print('# datapoints {:.2f} above {}'.format(num,df[df['Label'] > num].shape[0]))

def get_class(df,thres=0.7,delete=True):
    '''turns relKa into a 0,1 label wrt. a threshold'''
    df['Class'] = 0
    df.loc[df['Label']>=thres,'Class'] = 1
    if delete:
        df.drop(['Label'],axis=1,inplace=True)
        return df
    return df

def get_part_features(df,intra=0,inter=0,zW=0,zC=0):
    '''reduces the date to a specific data soruce like inter, intra etc.'''
    data_list = []
    nbp = 14
    for i in df.index:
        features = np.empty(0)
        cg = df.loc[i,:df.columns[-3]]
        tmp = np.reshape(cg.values,(4*nbp-3,6))


        if intra:
            tmp2 = tmp[0::4,:]
            intra_data = np.reshape(tmp2,(1,tmp2.shape[0]*tmp2.shape[1]))
            features = np.concatenate((features,intra_data),axis=None)
        if inter:
            tmp2 = tmp[1::4,:]
            inter_data = np.reshape(tmp2,(1,tmp2.shape[0]*tmp2.shape[1]))
            features = np.concatenate((features,inter_data),axis=None)
        if zW:
            tmp2 = tmp[2::4,:]
            zW_data=np.reshape(tmp2,(1,tmp2.shape[0]*tmp2.shape[1]))
            features = np.concatenate((features,zW_data),axis=None)
        if zC:
            tmp2 = tmp[3::4,:]
            zC_data=np.reshape(tmp2,(1,tmp2.shape[0]*tmp2.shape[1]))
            features = np.concatenate((features,zC_data),axis=None)

        data_list.append(list(features))

    res = pd.DataFrame(data_list)
    res['Class'] = df['Class']
    res['Label'] = df['Label']

    return res

def get_normal(df,num1=0,num2=-1,feature=1):
    '''normalizes the data and calculates different features'''
    if 'Label' in df.columns:
        df.drop(['Label'],axis=1,inplace=True)

    data_sc = pd.DataFrame(StandardScaler().fit_transform(df.loc[:,df.columns[num1:num2]]))

    if feature:
        data_sc['Mean'] = df.loc[:,df.columns[num1:num2]].mean(axis=1)
        data_sc['Var'] = df.loc[:,df.columns[num1:num2]].var(axis=1)
        data_sc['Min'] = df.loc[:,df.columns[num1:num2]].min(axis=1)
        data_sc['Max'] = df.loc[:,df.columns[num1:num2]].max(axis=1)
        data_sc['Median'] = df.loc[:,df.columns[num1:num2]].median(axis=1)
        data_sc['Sum'] = df.loc[:,df.columns[num1:num2]].sum(axis=1)

    data_sc['Class'] = df['Class']
    return data_sc

def get_testdata(df):
    '''split data in train and test'''
    tfbs = df[df.Class == 1]
    ntfbs = df[df.Class == 0]
    if 'Label' in df.columns:
        df.drop(['Label'],axis=1,inplace=True)
    #shuffeled_data = non_tfbs.sample(frac=1, random_state=42)
    # prepare df_train
    train = ntfbs.sample(frac=0.8, random_state = 42)
    train.drop(['Class'],axis=1,inplace=True)

    tmp = ntfbs.loc[list(set(ntfbs.index)-set(train.index)),:]
    test = pd.concat([tmp,pd.DataFrame(tfbs)])
    test_class = test.loc[:,'Class'].copy()
    test.drop(['Class'],axis = 1,inplace=True)
    tfbs.drop(['Class'],axis=1, inplace=True)

    # return train set, test set, test class, df[Class==1]
    return train, test, test_class, tfbs

def get_prec(df):
    '''returns the percentage of outliers in the dataset'''
    return (df[df['Class']==1].shape[0]/df.shape[0])

def get_labels(y):
    '''turns the -1,1 label into a 0,1 label'''
    tmp = np.zeros((y.shape[0]))
    tmp[y == -1] = 1
    return tmp

# Isolation Forest
def get_isfo(X,n_es=1500,perc=0.1):
    '''train Isolation Forest'''
    isofo = IsolationForest(n_estimators = n_es, max_features = 1.0, max_samples=1.0, 
                            bootstrap=False, random_state=42, contamination = perc).fit(X)
    return isofo

# OneClassSVM
def get_ocsvm(X,bound=0.01,perc=0.1,gamma=0.001,kernel='linear',deg=1):
    '''train one class svm'''
    ocsvm = OneClassSVM(nu=bound,gamma=gamma,kernel=kernel,degree=deg).fit(X)
    return ocsvm

# Elliptic Envelop
def get_elliptic_env(df,sf=0.99,per=0.1):
    '''trains elliptic envelop'''
    cov = EllipticEnvelope(support_fraction = sf, contamination = per).fit(df)
    return cov

def get_eval(model,X,y_truth):
    '''evaluates the trained model'''
    y_pred = model.predict(X)
    n_errors = (get_labels(y_pred) != y_truth).sum()
    #print('Number of Errors {}'.format(n_errors))
    return y_pred, n_errors

def get_results(y_pred, y_test):
    '''returns confusion matrix'''
    y_pred = get_labels(y_pred)
    cm = confusion_matrix(y_pred,y_test)
    # get f1score
    precision,recall,f1, support  = precision_recall_fscore_support(y_pred,y_test, average='binary')
    return cm, precision, recall, f1

def plot_cm(y_test,y_pred):
    '''plots confusion matrix'''
    df_cm = pd.DataFrame(confusion_matrix(y_test,get_labels(y_pred)),index = ['inlier','outlier'],columns=['inlier','outlier'])
    fig, ax = plt.subplots(figsize=(8,6))
    sns.set(font_scale= 1.4) # for label size
    sns.heatmap(df_cm, annot=True, annot_kws={"size": 16},fmt='g',cbar=False)
    ax.set_ylim([0,2])
    plt.ylabel('Actual')
    plt.xlabel('Predicted')

def get_angle(v1,v2):
    # calculate angle between two vectors
    return np.dot(v1,v2)/(nl.norm(v1)*nl.norm(v2))

def get_var(df):
    # get angles for all datapoints
    # calculate variance
    ang = np.zeros((df.shape[0],df.shape[0]))

    for d1 in range(df.shape[0]):
        for d2 in range(d1,df.shape[0]):
            tmp = get_angle(df.iloc[d1,:],df.iloc[d2,:])
            ang[d1,d2] = tmp
            ang[d2,d1] = tmp

    return np.var(ang,axis=1)

def get_angle_plot(df,num=100):
    # plot variance and relKa
    var_data = df.sample(n=num)
    if 'Class' in var_data.columns:
        var_data.drop(['Class'],axis=1,inplace=True)
    y_test = var_data.Label
    var_data.drop(['Label'],axis=1,inplace=True)
    ang_var = get_var(var_data)

    plt.figure(figsize=(10,10))
    plt.scatter(y_test,ang_var)
    plt.ylim([-10*min(ang_var),10*min(ang_var)])

def get_gaussian(df):
    return multivariate_normal(mean = np.mean(df,axis = 0), cov=(np.cov(df.T)))

def get_f1(df,dist):
    x = dist.pdf(df)

    EpsF1 = []

    epsilons = [1e-1,1e-2,1e-3,1e-4,1e-5,1e-6,1e-7,1e-8,1e-9,1e-10, 1e-20, 1e-30, 1e-40, 1e-50, 1e-60, 1e-70, 1e-80, 1e-90, 1e-100, 1e-110, 1e-120,1e-130,1e-140]

    for e in range(len(epsilons)):
        eps = epsilons[e]
        pred = (x <= eps)
        f = f1_score(test_class, pred, average='binary')
        #print("F1 score on test", round(f,4), " with epsilon ", eps)
        EpsF1.append([eps, round(f,4)])
        #print(sum(pred))

    EpsF1df = pd.DataFrame(EpsF1, columns = ['epsilon', 'F1'])
    EpsF1df['F1'].plot()
    return EpsF1df

'''
# f1 score elliptic envolop
def get_f1(train,test,perc,y_truth):

    min_sf = 0.95
    max_sf = 0.99
    f1 = []

    for sf in np.arange(min_sf, max_sf, 0.05):
        cov = get_model(train,sf, perc)
        y_pred, n_error = get_eval(cov,test,y_truth)
    
    y_pred = get_lables(y_pred)
    precision,recall,fbeta_score, support  = precision_recall_fscore_support(y_truth, y_pred, average='binary')
    
    print("F1 score on test", round(fbeta_score,4), " with support_fraction ", sf)
    f1.append([sf, round(fbeta_score,4)])
    
    df1 = pd.DataFrame(EpsF1, columns = ['sf', 'f1'])
    return df1

# ocsvm
def get_f1(train,test,perc,y_truth):

    min_es = 0.1
    max_es = 0.3
    f1 = []

    for n in np.arange(min_es, max_es, 0.02):
        ocsvm = get_model(train, n_es=es, perc=perc)
        y_pred, n_error = get_eval(ocsvm,test,y_truth)

        y_pred = get_lables(y_pred)
        precision,recall,fbeta_score, support  = precision_recall_fscore_support(y_truth, y_pred, average='binary')

        print("F1 score on test", round(fbeta_score,4), " with support_fraction ", sf)
        f1.append([sf, round(fbeta_score,4)])

    df1 = pd.DataFrame(f1, columns = ['sf', 'f1'])
    return df1
