# Repo for classification problem based on xgboost
This project is based on this paper
[Mathelier et. al. 2016](https://doi.org/10.1016/j.cels.2016.07.001)

I tested in this project if cgDNAplus features can improve the classification of binding sites. 
