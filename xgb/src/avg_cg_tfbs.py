import os
import pandas as pd
import numpy as np
import scipy as sc
import csv
import sys
sys.path.append('../../cgDNAplus_py')
from classes.cgDNAclass import cgDNA
from modules import cgDNAUtils as tools


def calc_avg_cgdna():
    files = os.listdir('../data/TFBS_data/')
    # iterate over all files in the data/TFBS_data folder
    for item in files:
        # open file and extract all upper case letters
        with open('../data/TFBS_data/{}'.format(item)) as csvfile:
            data = csv.reader(csvfile,delimiter=',')
            uppers=[]
            for element in list(data)[0]:
                tmp = [l for l in element if l.isupper()]
                uppers.append(''.join(tmp))
        # put data into dataframe
        df = pd.DataFrame(uppers,columns=['TFBS'])
        # calulate ground state and stiffness
        cgdna = cgDNA(df.loc[0,'TFBS'])
        ground_state = cgdna.ground_state
        stiffness = cgdna.stiff

        for elm in df.loc[1:,'TFBS']:
            if 'X' not in elm and 'N' not in elm:
                ground_state += cgDNA(elm).ground_state
                stiffness += cgDNA(elm).stiff

        # divide by numnber of datapoints
        ground_state = ground_state/df.shape[0]
        stiffness = stiffness/df.shape[0]

        # save ground state and stiffness in data_avg_cgDNA_TFBS folder
        name = item[:-4]
        np.save(f'../data/data_avg_cgDNA_TFBS/gs_{name}.npy',ground_state)
        sc.sparse.save_npz(f'../data/data_avg_cgDNA_TFBS/stiff_{name}.npz',stiffness)


if __name__ =='__main__':
    calc_avg_cgdna()
