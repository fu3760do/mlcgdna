#! /bin/python

import pandas as pd
import numpy as np
import numpy.linalg as npl
import sys
import scipy as sc
import scipy.linalg as scl
import scipy.sparse.linalg as ssl
sys.path.append('../../cgDNAplus')
#sys.path.append('../cgDNAplus_py/moduels')
#sys.path.append('../cgDNAplus_py/classes')
from classes.cgDNAclass import cgDNA
from modules import cgDNAUtils as tools
import itertools
import random
from Bio.Seq import Seq
import matplotlib.pyplot as plt
import seaborn as sns
#import argparse

from bokeh.plotting import figure, show
from bokeh.io import output_notebook
from bokeh.palettes import Spectral11
output_notebook()

#parser = argparse.ArgumentParser()
#parser.add_argument('-n',default='10',help='sequence length')
#args = parser.parse_args()

def calc_random_dna(num,name,save):
# define alphabet
    alpha = ['A','T','C','G']
    # define flenquing sequence
    flst = [random.choices(alpha,k=5) for i in range(0,10)]
    fseq = [''.join(flst[i]) for i in range(0,len(flst))]
    if num <= 10:
        # define sequences of specific length
        input_lst = [alpha]*num
        lst = list(itertools.product(*input_lst))
        lseq = [''.join(lst[i]) for i in range(0,len(lst))]
    else:
        lseq = []
        while len(lseq) < 50000:
            #breakpoint()
            seq = ''.join(random.choices(alpha,k=num))
            if seq not in lseq:
                com = str(Seq(seq).complement())
                lseq.append(seq)
                lseq.append(com)

    res = pd.DataFrame(lseq,columns=['seq'])
    
    if save:
        res.to_csv(name,index=False)
    
    return res


def calc_sequence_pca(num,save=False):
    # define alphabet
    alpha = ['A','T','C','G']
    # define flenquing sequence
    flst = [random.choices(alpha,k=5) for i in range(0,10)]
    fseq = [''.join(flst[i]) for i in range(0,len(flst))]
    if num <= 10:
    # define sequences of specific length
        input_lst = [alpha]*num
        lst = list(itertools.product(*input_lst))
        lseq = [''.join(lst[i]) for i in range(0,len(lst))]
        gs = [list(cgDNA(random.sample(fseq,k=1)[0]+lseq[i]+random.sample(fseq,k=1)[0]).ground_state)[len(fseq[0])*24:-len(fseq[0])*24] for i in range(0,len(lseq))]
    else:
        lseq = []
        gs = []
        while len(lseq) < 1000:
            #breakpoint()
            seq = ''.join(random.choices(alpha,k=num))
            if seq not in lseq:
                com = str(Seq(seq).complement())
                lseq.append(seq)
                lseq.append(com)
                gs.append(list(cgDNA(random.sample(fseq,k=1)[0]+seq+random.sample(fseq,k=1)[0]).ground_state)[len(fseq[0])*24:-len(fseq[0])*24])
                gs.append(list(cgDNA(random.sample(fseq,k=1)[0]+com+random.sample(fseq,k=1)[0]).ground_state)[len(fseq[0])*24:-len(fseq[0])*24])
    # marginalise ground_state len(fseq*24)
    breakpoint()
    #df = pd.DataFrame(gs)
    data = np.array(gs)
    #subtract mean
    #df = df.sub(df.mean(axis=0),axis=1)
    data = data - data.mean(axis=0)
    # calculate covariance matrix X.T X
    #Cm = np.dot(df.values.T,df.values)
    # u s v.T fpr pca we would have vsu.T usv.T so v can be used to transfrom the vectors
    # Clauclate SVD
    u,s,vh = np.linalg.svd(data)

    if save:
        # save data
        with open('./data/pca/data_{}.npy'.format(str(num)), 'wb') as f:
            np.save(f,data)

        with open('./data/pca/umatrix_{}.npy'.format(str(num)), 'wb') as f:
            np.save(f,u)

        with open('./data/pca/smatrix_{}_.npy'.format(str(num)), 'wb') as f:
            np.save(f,s)

        with open('./data/pca/vhmatrix_{}_.npy'.format(str(num)), 'wb') as f:
            np.save(f,vh)
    return data, u, s, vh

def calc_svd(num,save):
    for i in  range(5,num):
        data = np.load('../data/pca/Cm_{}_matrix.npy'.format(i))
        cov = np.cov(data.T)
        u,s,vh = npl.svd(cov)
        
        if save:
            with open('./data/pca/cov_{}.npy'.format(str(num)), 'wb') as f:
                np.save(f,cov) 
            with open('./data/pca/umatrix_{}.npy'.format(str(num)), 'wb') as f:
                np.save(f,u)
            with open('./data/pca/smatrix_{}.npy'.format(str(num)), 'wb') as f:
                np.save(f,s)
            with open('./data/pca/vhmatrix_{}.npy'.format(str(num)), 'wb') as f:
                np.save(f,vh)

    return

def calc_eigs(num,save):
    #for i in range(5,num):
    data = np.load('./data/pca/data_{}.npy'.format(num))
    data = data - data.mean(axis=0)
    cov = np.cov(data.T)
    eigs, evecs = npl.eig(cov)

    if save:
        with open('./data/pca/cov_{}.npy'.format(str(num)), 'wb') as f:
            np.save(f,cov)
        with open('./data/pca/eigs_{}.npy'.format(str(num)), 'wb') as f:
            np.save(f,eigs)
        with open('./data/pca/evecs_{}.npy'.format(str(num)), 'wb') as f:
            np.save(f,evecs)

    return

def get_projm(num):
    # function to create pca projection matrices
    tmp = []
    for i in range(5,num):
        eigs = np.load('../data/pca/eigs_{}.npy'.format(i))
        evecs = np.load('../data/pca/evecs_{}.npy'.format(i))
        var = []
        for k in eigs:
            var.append((k/sum(eigs))*100)
        # Search for the first eigenval which explain 95 or more (list[1] dirty
        # trick to get elm +1)
        vals = [j for j,x in enumerate(np.cumsum(var)) if x > 95][1]
        tmp.append(vals/eigs.shape[0])
        # 0.1 is the mean of tmp
        projm = (evecs.T[:][:int(eigs.shape[0]*0.1)]).T
        
        with open('../data/pca/projm_{}.npy'.format(str(i)), 'wb') as f:
            np.save(f,projm)

    return tmp


def plot_svd_eigs(num):
    for i in range(5,num):
        data = np.load('../data/pca/data_{}.npy'.format(i))
        u,s_data,vh = npl.svd(data.T)
        eigs = (s_data*s_data)/(len(s_data)-1)
        p = figure(title='Eigen values of Cm{}'.format(i),x_axis_label='Number of eigen value', y_axis_label='Value',plot_width=800,plot_height=800)
        p.circle(list(range(0,len(eigs))), eigs,color='blue')
        show(p)

def plot_eigs(num):
    for i in range(5,num):
        x_data = np.load('../data/pca/data_{}.npy'.format(i))
        #x_data = x_data - x_data.mean(axis=0);
        #cm = x_data.T@x_data/(x_data.shape[0]-1)
        cm = np.cov(x_data.T)
        v,w = np.linalg.eig(cm)
        p = figure(title='Eigen value of Cm{} '.format(i), x_axis_label='Number of eigenvalue', y_axis_label='Value',plot_width=800,plot_height=800)
        p.circle(list(range(0,len(v))), v,color='blue')
        show(p)

def plot_cov(num):
    for i in range(5,num):
        data = np.load('../data/pca/data_{}.npy'.format(i))
        #data = data - data.mean(axis=0); cov = data.T@data/(data.shape[0]-1)
        cov = np.cov(data.T)
        fig = plt.figure(figsize=(12,12))
        sns.heatmap(cov,cmap="RdBu",center=0, square=True)
        plt.show()

def Kullback_Leibler(cg1,tfbs_gs,tfbs_stiff):
    breakpoint()
    p1 = sc.log(scl.det(tfbs_stiff.todense())/scl.det(cg1.stiff.todense())) - len(cg1.ground_state)
    p2 = sc.trace(sc.dot(ssl.inv(tfbs_stiff).todense(),cg1.stiff.todense()))
    p3 = sc.dot((tfbs_gs-cg1.ground_state),sc.dot(ssl.inv(tfbs_stiff).todense(),(tfbs_gs-cg1.ground_state)).T)
    return 1/2*(p1+p2+p3[0,0])

if __name__ == '__main__':
    #calc_sequence_pca(11)
    calc_svd(int(args.n),True)
