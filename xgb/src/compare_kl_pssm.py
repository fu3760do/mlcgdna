import Bio
import Bio.SeqIO
import pickle
import scipy as sc
import pandas as pd
import numpy as np
import scipy.sparse as scs
import scipy.linalg as sl
import cgutils as ut
import pandas_bokeh
import math
from operator import itemgetter
from bokeh.palettes import Spectral11
from bokeh.plotting import figure, show, output_file
#output_file("line.html")


def KL_sym(mu1, cv1, mu2, cv2):
        N = len(mu1)
        mu12 = np.subtract(mu1,mu2)
        d = (np.trace(sl.solve(cv1,cv2)) + np.trace(sl.solve(cv2,cv1)) \
             - 2*N + np.matmul(np.matmul(mu12.T,np.add(sl.inv(cv1),sl.inv(cv2))),mu12))/4
        per_dof = d/N
        return per_dof


def compare_kl_pssm_seq(seq,tfbs):
    # load avg ground stata and stiffness
    with open('../data/PSSM/'+tfbs + '.pkl', 'rb') as f:
        pssm = pickle.load(f)
    
    tfbs_gs = np.load(f'../data/data_avg_cgDNA_TFBS/gs_{tfbs}.npy')
    tfbs_stiff = scs.load_npz(f'../data/data_avg_cgDNA_TFBS/stiff_{tfbs}.npz')
    df = pd.DataFrame()
    df['pssm'] = (pssm.calculate(seq)-pssm.min) / (pssm.max-pssm.min)
    scores = [(pos, ((score - pssm.min) / (pssm.max - pssm.min)))
     for pos, score in pssm.search(seq, pssm.min) if not
     math.isnan(score)]
    if scores:
        # itemgetter(1) searches for max in the second position of the tuple
        pos_maxi, maxi = max(scores, key=itemgetter(1))
        strand = "+" 
        if pos_maxi < 0:
            strand = "-" 
            pos_maxi = pos_maxi + len(seq)
        pos_len = pos_maxi + pssm.length

    kl_lst = []
    pssm_lst = []
    tfbs_len = int((len(tfbs_gs)+18)/24)
    for i in range(0,len(seq)-tfbs_len+1):
        subseq = seq[i:i+tfbs_len]
        cgdna = ut.cgDNA(subseq)
        #pssm_lst = pssm.calculate(subseq)
        kl_lst.append(KL_sym(cgdna.ground_state,cgdna.stiff.todense(),tfbs_gs,tfbs_stiff.todense()))    
    df['kl'] = kl_lst
    
    return df, pos_maxi, pos_len


def plot_kl_pssm(df,pssm_max):
    print(df.corr())
    numlines=len(df.columns)
    mypalette=Spectral11[0:numlines]

    p = figure(plot_width=800, plot_height=250)
    for data, name, color in zip([df['pssm'], df['kl']], ["pssm", "kl"], mypalette):
        tmp = pd.DataFrame(data)
        p.line(tmp.index.values,tmp[name].values, line_width=2, color=color, alpha=0.8, legend_label=name)

    p.circle(pos_max,df.loc[pssm_max,'pssm'],size=20, color="red", alpha=0.5)
    p.legend.location = "top_left"
    p.legend.click_policy="hide"

    show(p)


if __name__=='__main__':
    seq = 'CAAAACGGGAATGTATGGTAATCCGTGCAGGGGTAACTGTTCACTTTAGGCTCATATTGCGTAAACATACCTTCTCAGCAACTGGAAGATCCA'
    motif = 'MA0473.1'
    dfpk, pos_max, pos_len = compare_kl_pssm_seq(seq,motif)
    plot_kl_pssm(dfpk,pos_max)
