#!/usr/bin/python

import sys
n = 0
with open(sys.argv[1], 'r') as f:
    with open(sys.argv[2], 'w') as out:
        for i,line in enumerate(f):
            if i>2:
                n+=1
                out.write('>' + str(n) + '\n' + line.strip())
