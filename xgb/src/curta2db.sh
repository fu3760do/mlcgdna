#! /bin/sh


path=~/Dokumente/Projekte/mlcgdna/xgb/res/
file=$path$1
folder="$(echo $1 | cut -d'.' -f1)"
if [ -d "$path$folder" ]; then
	echo "Folder for $1 exisits" 
else
	# copy data from curta
	scp fu3760do@curta.zedat.fu-berlin.de:~/cgDNA/mlcgdna/xgb/res/$1 $path

	# extract data
	tar -xzf $file -C $path

	# include the results into the results db
	tablename=xgb_mathelier_$folder
	python ./results2db.py -n $tablename -p $path -f $folder/results
fi



