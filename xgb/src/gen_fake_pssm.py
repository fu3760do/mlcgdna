#!\bin\python

import pandas as pd
import numpy as np
import pickle

rng = np.random.default_rng(12345)

def invert_sampling(pssm,pos):
    alphabet = ['A','C','G','T']
    prob = [[letter,2**pssm[letter][pos]*0.25] for letter in alphabet]
    prob.sort(key=lambda x:x[1])
    tmp1 = prob[0][1]
    prob[0][1] = prob[3][1]
    prob[3][1] = tmp1

    tmp2 = prob[1][1]
    prob[1][1] = prob[2][1]
    prob[2][1] = tmp2

    return prob

def get_fake_seq(num_seq,seq_len,motif,save=False):
    with open(f'../data/PSSM/{motif}.pkl', 'rb') as f:
        pssm = pickle.load(f)
    cols = [f'pos_{i}' for i in range(0,seq_len)]
    df = pd.DataFrame(np.zeros([num_seq,seq_len]),columns=cols)
    dfh = pd.DataFrame(np.zeros([num_seq,4]))
    for i in range(0,seq_len):
        randn = pd.Series(np.random.rand(num_seq))
        prob = invert_sampling(pssm,i%pssm.length)
        prob_old = 0; prob_new = 0
        inc = ['both','right','right','right']
        for j in range(0,4):
            prob_new += prob[j][1] 
            dfh.loc[:,j] = randn.between(prob_old,prob_new,inclusive=inc[j])
            prob_old = prob_new
        for k in range(0,4):
            df.loc[dfh.loc[:,k],f'pos_{i}'] = prob[k][0]
            
    fake_seq = []
    for index in df.index:
        test = ''
        for col in df.columns:
            test += df.loc[index,col]
        if test not in fake_seq:
            fake_seq.append(test)
    if save:
        pd.DataFrame(fake_seq).to_csv(f'../data/fake_{motif}_data.txt',index=False,header=False)
    
    return fake_seq

if __name__ == '__main__':
    get_fake_seq(100,1000,'MA0473.1')
