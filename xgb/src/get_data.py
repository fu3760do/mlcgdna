#! /bin/python

import os, sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import scipy as sc
import scipy.linalg as sl

import cgutils as ut

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-f', default= 'fore_debug.fa', help = 'name of the tfbs file')
parser.add_argument('-m', default = 'MA0003.2', help =' motif as sting')
args = parser.parse_args()

in_path = '/scratch/fu3760do/cgdna/xgb/data/cgdata'
out_path = '/scratch/fu3760do/cgdna/xgb/data/cgdata'
tfbs_path = '/scratch/fu3760do/cgdna/xgb/data/data_avg_cgDNA_TFBS'

def KL_sym(mu1, cv1, mu2, cv2):
    N = len(mu1)
    mu12 = np.subtract(mu1,mu2)
    d = (np.trace(sl.solve(cv1,cv2)) + np.trace(sl.solve(cv2,cv1)) - 2*N
         +np.matmul(np.matmul(mu12.T,np.add(sl.inv(cv1),sl.inv(cv2))),mu12))/4
    per_dof = d/N
    return per_dof


def mahalanobis(x, tfbs_mu, tfbs_stiff):
    """Compute the Mahalanobis Distance between each row of x and the data
    x    : vector or matrix of data with, say, p columns.
    tfbs_mu : ndarray of the distribution from which Mahalanobis distance of each observation of x is to be computed.
    tfbs_stiff : covariance matrix (p x p) of the distribution. If None, will be computed from data.
    """
    x_minus_mu = x - tfbs_mu
    inv_covmat = np.linalg.inv(tfbs_stiff) # not sure what stiffness is
    left_term = np.dot(x_minus_mu, inv_covmat)
    mahal = np.dot(left_term, x_minus_mu)
    return mahal.item()


def get_col_names(tfbs_gs):
    nbp = int((len(tfbs_gs)+18)/24)
    label=['seq','pssm','kl_sym','mah']
    for i in range(nbp-1):
        tmp1=['y_'+str(i+1) for i in range(i*6,(i+1)*6)]
        tmp2=['zC_'+str(i+1) for i in range(i*6,(i+1)*6)]
        tmp3=['x_'+str(i+1) for i in range(i*6,(i+1)*6)]
        tmp4=['zW_'+str(i+1) for i in range(i*6,(i+1)*6)]
        label += (tmp1+tmp2+tmp3+tmp4)
    tmp1=['y_'+str(i+1) for i in range((nbp-1)*6,nbp*6)]
    label += tmp1
    return label


def get_data(name,tfbs):
    lst = []
    tfbs_gs = np.load(f'../data/data_avg_cgDNA_TFBS/gs_{tfbs}.npy')
    tfbs_stiff = sc.sparse.load_npz(f'../data/data_avg_cgDNA_TFBS/stiff_{tfbs}.npz')
    df = pd.read_csv(f"../data/data_{name}.csv")

    for i in range(0,10):
        seq = (df.loc[i,'seq'][df.loc[i,'pos_max+1']-1:df.loc[i,'pos_max+pssm.len']]).upper()
        pssm = df.loc[i,'pssm']
        cgdna = ut.cgDNA(seq)
        kl = KL_sym(cgdna.ground_state,cgdna.stiff.todense(),tfbs_gs,tfbs_stiff.todense())
        mah = mahalanobis(cgdna.ground_state,tfbs_gs,tfbs_stiff.todense())
        tmp = [seq,pssm,kl,mah]
        for item in cgdna.ground_state:
            tmp.append(item)
        lst.append(tuple(tmp))

    # plot and save data
    names = get_col_names(tfbs_gs)
    res = pd.DataFrame(lst,columns=names)
    res.to_csv(f'../data/cgdata/data_{name}.csv',index=False)
    corr = res.loc[:, res.columns[1:-1]].corr()
    corr.to_csv(f'../data/cgdata/corr_{name}.csv',index=False)
    #img = sns.heatmap(corr, cmap="RdBu",center=0)
    #figure = img.get_figure()
    #figure.savefig(f'../doc/hist_{name}.jpg')


def append_mahalanobis(name,tfbs):
    lst = []
    tfbs_gs = np.load(f'{tfbs_path}/gs_{tfbs}.npy')
    tfbs_stiff = sc.sparse.load_npz(f'{tfbs_path}/data_avg_cgDNA_TFBS/stiff_{tfbs}.npz')
    df = pd.read_csv(f"{in_path}/data_{name}.csv")

    for s in df['seq']:
        cgdna = ut.cgDNA(s)
        mah = mahalanobis(cgdna.ground_state,tfbs_gs,tfbs_stiff.todense())
        lst.append(mah)

    df['mah'] = lst
    df.to_csv(f'{out_path}/data_{name}.csv',index=False)
    corr = df.loc[:, df.columns[1:-1]].corr()
    corr.to_csv(f'{out_path}/corr_{name}.csv',index=False)
    #img = sns.heatmap(corr, cmap="RdBu",center=0)
    #figure = img.get_figure()
    #figure.savefig(f'../doc/hist_{name}.jpg')


if __name__ == '__main__':
    name = args.f
    tfbs = args.m
    append_mahalanobis(name,tfbs)

