import requests
from bs4 import BeautifulSoup


def check_jaspar():
    # loop of all jasper matrix sites
    for i in range(1,2):
        try:
            # request all matricies on page i
            data = \
            requests.get(f'http://jaspar.genereg.net/api/v1/matrix?page={i}').json()
            # loop over all objects which are in the object
            for i in range(0,len(data['results'])):
                # get the name of the matirx
                name = data['results'][i]['matrix_id']
                # get the matrix info
                motif = \
                requests.get(f"http://jaspar.genereg.net/api/v1/matrix/{name}").json()
                # request the data on the html site for the motif
                red_seq = []; len_seq = []
                response = requests.get(f"http://jaspar.genereg.net/sites/{name}/",timeout=100)
                if response.status_code == 200: 
                    # scrape all the html lines
                    res = BeautifulSoup(response.text,features="lxml")
                    tmp =  list(set(res.find_all('pre')))
                    # loop over all objects which have a pre htlm
                    for elm in tmp:
                        fseq = []
                        for item in elm:
                            item = str(item)
                            # cut out all red colored letters
                            if "red" in item: # red in string
                                # wirte all red letters in a list
                                red_seq.append(item[21:-11]) 
                                # get len and append to a list
                                len_seq.append(len(item[21:-11]))
                # calcualte the average of all sequences
                avg_len = sum(len_seq) / len(len_seq)
                # write all datapoints to a file
                with open('Results_Jaspar.txt','a') as f:
                    f.write(f"Name of the Motif: \t {name} \n")
                    f.write(f"Length of pfm[A]: \t {len(motif['pfm']['A'])} \n")
                    f.write(f"Length of pfm[C]: \t {len(motif['pfm']['C'])} \n")
                    f.write(f"Length of pfm[G]: \t {len(motif['pfm']['G'])} \n")
                    f.write(f"Length of pfm[T]: \t {len(motif['pfm']['T'])} \n")
                    f.write(f'Avg len of the red sequence {avg_len} \n')
                    f.write('--------------------------------------------------- \n')
        except:
            pass

if __name__ == '__main__':
    check_jaspar()
