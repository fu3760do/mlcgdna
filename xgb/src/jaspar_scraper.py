#! /bin/python

from bs4 import BeautifulSoup
import pandas as pd
import requests
import csv
import os

def write2csv(lst,name):
    with open('../data/TFBS_data/{}.txt'.format(name),'a') as file:
        csv_writer = csv.writer(file)
        csv_writer.writerow(lst)

def get_tfbs_data(name,fsqelen=5):
    res = []
    try:
        response = requests.get('http://jaspar.genereg.net/sites/{}/'.format(name),timeout=50)
        if response.status_code == 200: 
            data = BeautifulSoup(response.text)
            tmp =  list(set(data.find_all('pre')))
            for elm in tmp:
                fseq = []
                for item in elm:
                    item = str(item)
                    if "grey" in item: 
                        fseq.append(item[19:-7])
                    if "red" in item: # red in string
                        iseq = item[21:-11] 

                if len(fseq) > 1:
                    res.append(fseq[0][-fsqelen:] + iseq + fseq[1][:fsqelen])
        else:
            return [name]
    except:
        return [name]
    return res

if __name__=='__main__':
    df = pd.read_csv('../data/used_TFs_clean_len.csv')
    tfbs_list=set(df['JASPAR2014 matrix identifier'])
    no_work = []
    for elm in tfbs_list:
        tmp = get_tfbs_data(elm)
        if len(tmp) > 1:
            write2csv(tmp,elm)
        else:
            no_work.append(elm[0])
