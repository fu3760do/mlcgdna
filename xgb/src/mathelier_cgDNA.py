import pandas as pd
import numpy as np
import os
import json
import pickle
import math
from operator import itemgetter
import scipy.sparse as scs 
import scipy.linalg as sl


from sklearn.model_selection import train_test_split
from xgboost import XGBClassifier
import xgboost as xgb

import Bio
import Bio.SeqIO

import sys
sys.path.append('../../cgDNAplus')
from classes.cgDNAclass import cgDNA
from modules import cgDNAUtils as tools

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-f', default= 'fore_debug.fa', help = 'name of the tfbs file')
parser.add_argument('-m', default = 'MA00000', help =' motif as sting')
parser.add_argument('-e', default = 'pssm', help = 'specify encoding')
parser.add_argument('-t', default = '', help = 'tag')
parser.add_argument('-s', default = False, help= 'save input data as csv')
args = parser.parse_args()

def save_obj(obj,name):
    with open('../data/PSSM/'+ name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name):
    with open('../data/PSSM/'+name + '.pkl', 'rb') as f:
        return pickle.load(f)

def KL_sym(mu1, cv1, mu2, cv2):
        N = len(mu1)
        mu12 = np.subtract(mu1,mu2)
        d = (np.trace(sl.solve(cv1,cv2)) + np.trace(sl.solve(cv2,cv1)) \
             - 2*N + np.matmul(np.matmul(mu12.T,np.add(sl.inv(cv1),sl.inv(cv2))),mu12))/4
        per_dof = d/N
        return [per_dof]

def find_pssm_hits(motif, seq_file,encode='pssm'):
    pssm = load_obj(motif)
    """ Predict hits in sequences using a PSSM. """
    if 'pca' in encode:
        vpca = np.load('../data/pca/projm_{}.npy'.format(pssm.length-1))
    if 'kl' in encode:
        tfbs_gs = np.load(f'../data/data_avg_cgDNA_TFBS/gs_{motif}.npy')
        tfbs_stiff = scs.load_npz(f'../data/data_avg_cgDNA_TFBS/stiff_{motif}.npz')

    hits = []
    for record in Bio.SeqIO.parse(seq_file, "fasta"):

        tmp = []
        # pssm.search find hits with PWM score above pssm.min
        scores = [(pos, ((score - pssm.min) / (pssm.max - pssm.min)))
                  for pos, score in pssm.search(record.seq, pssm.min) if not
                  math.isnan(score)]
        # find the max score in all found scores
        if scores:
            # itemgetter(1) searches for max in the second position of the tuple
            pos_maxi, maxi = max(scores, key=itemgetter(1))
            strand = "+"
            if pos_maxi < 0:
                strand = "-"
                pos_maxi = pos_maxi + len(record.seq)
            tmp=[str(record.seq), pos_maxi + 1, pos_maxi + pssm.length, strand, maxi]
            if 'pca' in encode:
                cg_data =np.array(get_features('cg',\
                            str(record.seq)[pos_maxi+1:pos_maxi+pssm.length].upper()))
                trans_data = cg_data@vpca
                tmp.extend(trans_data)
            elif 'kl' in encode:
                cg_data = get_features('cg_all',str(record.seq)\
                                       [pos_maxi:pos_maxi+pssm.length].upper())
                kl = KL_sym(cg_data.ground_state,cg_data.stiff.todense(),\
                                  tfbs_gs,tfbs_stiff.todense())
                tmp.extend(kl)

            else:
                tmp.extend(get_features(encode,str(record.seq)[pos_maxi+1:pos_maxi+pssm.length].upper()))
        hits.append(tmp)

    return hits

def eval_model(model, data, path, name='None', encode='pssm',tag=''):
    lst_enc = encode.split('+')
    enc = '_'.join(lst_enc)
    folder_name = 'results_'+enc
    if tag:
        folder_name +='_'+tag
    make_folder(path,folder_name)


    res, fimp = {}, []
    gtrain, gtest = 0, 0

    cols = list(data.columns.values)
    cols.remove('Class')


    res = {'name':name,'encode':encode}
    num = 10.0
    for i in range(int(num)):
        # Get training and test data
        Xtrain, Xtest, ytrain, ytest = train_test_split(data, data['Class'], \
                        stratify = data['Class'],test_size=0.3, random_state=i)
        # drop class
        Xtrain = Xtrain.drop(['Class'], axis=1)
        Xtest = Xtest.drop(['Class'], axis=1)
        # get model
        model.fit(Xtrain,ytrain,eval_set=[(Xtrain,ytrain),(Xtest,ytest)],\
                  verbose=0,early_stopping_rounds=100)
        train_aucpr = max(model.evals_result()['validation_0']['aucpr'])
        test_aucpr = max(model.evals_result()['validation_1']['aucpr'])
        gtrain += train_aucpr
        gtest += test_aucpr
        res['train_aucpr_'+str(i)] = train_aucpr
        res['test_aucpr_'+str(i)] = test_aucpr

        fimp.append(list(model.feature_importances_))

    res['mean_train_aucpr'] = gtrain/num
    res['mean_test_aucpr'] = gtest/num

    tmp_fimp = pd.DataFrame(fimp,columns=cols)

    file_name = name +'_'+encode+'_'+tag+'.json'
    with open(os.path.join(os.path.join(path,folder_name+'/results'),file_name),'w') as file:
        json.dump(res,file)

    file_name = name +'_'+encode+'_'+tag+'.csv'
    tmp_fimp.to_csv(os.path.join(os.path.join(path,folder_name+'/features_importance'),file_name))

    return res, tmp_fimp

def get_data(foreground,background,motif,encode='pssm'):

    pssm = load_obj(motif)
    nbp = pssm.length-1
    label= ['seq','pos_max+1','pos_max+pssm.len','strand','pssm']
    label.extend(get_names(encode,nbp))

    fore_hits = find_pssm_hits(motif,foreground,encode=encode)
    fore = pd.DataFrame(fore_hits,columns=label)
    fore['Class'] = 1
 
    back_hits = find_pssm_hits(motif,background,encode=encode)
    back = pd.DataFrame(back_hits,columns=label)
    back['Class'] = 0

    return pd.concat([fore,back],ignore_index=True)

def get_names(feat,nbp):
    label = []
    lst_feat = feat.split('+')
    if 'mat' in lst_feat:
        label += ['Roll_'+str(i) for i in range(0,nbp-1)]
        label += ['HeIT_'+str(i) for i in range(0,nbp-1)]
        label += ['ProT_'+str(i) for i in range(0,nbp)]
        label += ['MGW_'+str(i) for i in range(0,nbp-2)]

    elif 'mgw' in lst_feat:
        label += ['Roll_'+str(i) for i in range(0,nbp-1)]
        label += ['HeIT_'+str(i) for i in range(0,nbp-1)]
        label += ['ProT_'+str(i) for i in range(0,nbp)]
        label += ['MGW_min','MGW_max']

        if 'buckle' in lst_feat:
            label += ['Buckle_'+str(i) for i in range(0,nbp)]

        elif 'open' in lst_feat:
            label += ['Opening_'+str(i) for i in range(0,nbp)]

        elif 'shear' in lst_feat:
            label += ['Shear_'+str(i) for i in range(0,nbp)]

        elif 'strech' in lst_feat:
            label += ['Strech_'+str(i) for i in range(0,nbp)]
        
        elif 'stager' in lst_feat:
            label += ['Stager_'+str(i) for i in range(0,nbp)]
        
        elif 'tilt' in lst_feat:
            label += ['Tilt_'+str(i) for i in range(0,nbp-1)]

        elif 'shift' in lst_feat:
            label += ['Shift_'+str(i) for i in range(0,nbp-1)]

        elif 'slide' in lst_feat:
            label += ['Slide_'+str(i) for i in range(0,nbp-1)]

        elif 'rise' in lst_feat:
            label += ['Rise_'+str(i) for i in range(0,nbp-1)]


    elif 'cg' in lst_feat:
        for i in range(nbp-1):
            tmp1=['y_'+str(i+1) for i in range(i*6,(i+1)*6)]
            tmp2=['zC_'+str(i+1) for i in range(i*6,(i+1)*6)]
            tmp3=['x_'+str(i+1) for i in range(i*6,(i+1)*6)]
            tmp4=['zW_'+str(i+1) for i in range(i*6,(i+1)*6)]
            label += (tmp1+tmp2+tmp3+tmp4)
        tmp1=['y_'+str(i+1) for i in range((nbp-1)*6,nbp*6)]
        label += tmp1

    elif 'kl' in lst_feat:
        label = ['kl']

    elif 'intras' in lst_feat:
        label = ['intra_'+str(i) for i in range(6*nbp)]

    elif 'inters' in lst_feat:
        label = ['inter_'+str(i) for i in range(6*(nbp-1))]

    elif 'zW' in lst_feat:
        label = ['zW_'+str(i) for i in range(6*(nbp-1))]

    elif 'zC' in lst_feat:
        label = ['zC_'+str(i) for i in range(6*(nbp-1))]

    elif 'pca' in lst_feat:
        label = ['pca_'+str(i) for i in range(int((24*nbp-18)*0.1))]

    elif 'dnashape' in lst_feat and 'small' not in lst_feat:
        for i in range(nbp-4):
            name =['MGW_'+str(i),'ProT_'+str(i),'Roll_'+str(i),'HelT_'+str(i),\
                   'Stretch_'+str(i),'Tilt_'+str(i),'Buckle_'+str(i),'Shear_'+str(i),\
                   'Opening_'+str(i),'Rise_'+str(i),'Shift_'+str(i),'Stagger_'+str(i),\
                   'Slide_'+str(i)]
            label += name
        ntmp = nbp-4
        label += ['Roll_'+str(ntmp),'HelT_'+str(ntmp),'Tilt_'+str(ntmp),\
                  'Rise_'+str(ntmp),'Shift_'+str(ntmp),'Slide_'+str(ntmp)]

    elif 'small' in lst_feat: # dnashape+small
        for i in range(nbp-4):
            name = ['MGW_'+str(i),'ProT_'+str(i),'Roll_'+str(i),'HelT_'+str(i)]
            label += name
        ntmp = nbp-4
        label += ['Roll_'+str(ntmp),'HelT_'+str(ntmp)]

    elif 'pssm' in lst_feat:
        label = []

    return label

def get_features(feat,seq):
    tmp = []
    lst_feat = feat.split('+')
    if 'mat' in lst_feat:
        cg = cgDNA(seq).ground_state
        # add roll/Roll
        tmp.extend(list(cg[7:len(cg):24]))
        # add  twist/HeIT
        tmp.extend(list(cg[8:len(cg):24]))
        # add propellor/ProT
        tmp.extend(list(cg[1:len(cg):24]))
        # add groovewidth
        tmp.extend(tools.GrooveWidths(cg,True))

    elif 'mgw' in lst_feat:
        cg = cgDNA(seq).ground_state
        # add roll/Roll
        tmp.extend(list(cg[7:len(cg):24]))
        # add  twist/HeIT
        tmp.extend(list(cg[8:len(cg):24]))
        # add propellor/ProT
        tmp.extend(list(cg[1:len(cg):24]))
        # add groovewidth
        tmp.extend(tools.GrooveWidths(cg,False))

        if 'buckle' in lst_feat:
            # add buckle
            tmp.extend(list(cg[0:len(cg):24]))

        elif 'open' in lst_feat:
            # add opening
            tmp.extend(list(cg[2:len(cg):24]))
    
        elif 'shear' in lst_feat:
            # add shear
            tmp.extend(list(cg[3:len(cg):24]))

        elif 'strech' in lst_feat:
            # add strech
            tmp.extend(list(cg[4:len(cg):24]))

        elif 'stager' in lst_feat:
            # add stager
            tmp.extend(list(cg[5:len(cg):24]))

        elif 'tilt' in lst_feat:
            # add tilt
            tmp.extend(list(cg[6:len(cg):24]))

        elif 'shift' in lst_feat:
            # add shift
            tmp.extend(list(cg[9:len(cg):24]))

        elif 'slide' in lst_feat:
            # add slide
            tmp.extend(list(cg[10:len(cg):24]))

        elif 'rise' in lst_feat:
            # add rise
            tmp.extend(list(cg[11:len(cg):24]))

    elif 'cg' in lst_feat:
        # get cgdna ground_state
        tmp = list(cgDNA(seq).ground_state)

    elif 'cg_all' in lst_feat:
        # get cgdna ground state and stiffness
        tmp = cgDNA(seq)

    elif 'pssm' in feat:
        tmp=[]

    elif 'intras' in lst_feat:
        cg = cgDNA(seq).ground_state
        nbp = len(seq)
        intras = np.reshape(cg,(4*nbp-3,6))[0::4,:]
        tmp = np.reshape(intras,(1,intras.shape[0]*intras.shape[1])).tolist()[0]

    elif 'inters' in lst_feat:
        cg = cgDNA(seq).ground_state
        nbp = len(seq)
        inters = np.reshape(cg,(4*nbp-3,6))[1::4,:]
        tmp = np.reshape(inters,(1,inters.shape[0]*inters.shape[1])).tolist()[0]

    elif 'zW' in lst_feat:
        cg = cgDNA(seq).ground_state
        nbp = len(seq)
        zW = np.reshape(cg,(4*nbp-3,6))[2::4,:]
        tmp = np.reshape(zW,(1,zW.shape[0]*zW.shape[1])).tolist()[0]

    elif 'zC' in lst_feat:
        cg = cgDNA(seq).ground_state
        nbp = len(seq)
        zC = np.reshape(cg,(4*nbp-3,6))[3::4,:]
        tmp = np.reshape(zC,(1,zC.shape[0]*zC.shape[1])).tolist()[0]

    elif 'dnashape' in lst_feat:
        table_file = 'DNA_shape_full_sym_table_updated.csv'
        query_table = pd.read_csv(table_file,index_col='Pentamer')
        
        N_seq = len(seq)
        n_features = 13 #columns=['MGW',ProT','Roll','HelT','Stretch','Tilt','Buckle','Shear','Opening','Rise','Shift','Stagger','Slide'])

        #Init feature array
        array_zero = np.empty((N_seq,n_features))
        array_zero[:] = np.NaN
        seq_char = list(seq)
        shape_table = np.zeros((n_features, N_seq))

        main_indices = [0,1,2,4,6,7,9,10,11,12,14,16,17]# Indices of intra coord + first inter coord in pentamer table
        second_indices =  [3,5,8,13,15,18] # Indices second inter coord in pentamer table
        intra_ind = [0,1,4,6,7,8,11] #indices of intra coord in feature table
        inter_ind =  [2,3,5,9,10,12] #indices of inter coord in feature table

        #Build feature array
        for i in range(0,N_seq - 4):

            current_pent = seq[i:i+5]
            current_feat = query_table.loc[current_pent]

            shape_table[:,i] += current_feat[main_indices]
            shape_table[inter_ind,i+1] += current_feat[second_indices]

        #Averaging of pentamer block contributions for inter coordinates
        for i in inter_ind:
            shape_table[i,1:N_seq-4] = shape_table[i,1:N_seq-4]/2

        # Replace 4 last (for intras) or 3 last (for inters) values with NaN (they correpond to missing values at both ends of sequence)
        shape_table[intra_ind,-4:] = np.nan
        shape_table[inter_ind,-3:] = np.nan

        if 'small' in lst_feat:
            shape_table = shape_table[0:4,:]

        # Unravel feature table columns into a single vector, get rid of nan entries
        # The last 5 mer only contributes columns=['Roll','HelT','Tilt','Rise','Shift','Slide'])
        shape_vector_long = shape_table.flatten('F')
        tmp = np.delete(shape_vector_long , np.where(np.isnan(shape_vector_long) ))

    return tmp

def make_folder(path,folder):
    folder = os.path.join(path,folder)
    if not os.path.exists(folder):
        os.mkdir(folder)
        os.mkdir(os.path.join(folder,'results'))
        os.mkdir(os.path.join(folder,'features_importance'))

    return

if __name__ == '__main__':
    name = args.f
    forefa_file= '../data/mat_data/foreground/fore_{}.fa'.format(name)
    backfa_file = '../data/mat_data/background/back_{}.fa'.format(name)
    motif_str = args.m
    data = get_data(forefa_file,backfa_file,motif_str,encode=args.e)
    if args.s:
        # adapt path to save the data
        data.to_csv(f'../data/cutouts/data_{name}_{motif_str}.csv'.format(name))
    tf_data = data.loc[:,list(data.columns)[4:]]
    model = XGBClassifier(eval_metric='aucpr')
    # set different path for new experiments
    eval_model(model,tf_data,'../res/',name,args.e,tag=args.t)

#   debug = python mathelier_cgDNA.py -f wgEncodeAwgTfbsHaibA549Creb1sc240V0416102Dex100nmUniPk_debug -m MA0018.2 -e pssm+kl


