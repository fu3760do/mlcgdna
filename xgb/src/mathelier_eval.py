#! /bin/python

import pandas as pd
import numpy as np
import os
import json
import pickle
import sqlite3
import pandas_bokeh
pandas_bokeh.output_notebook()

from bokeh.plotting import figure, show
from bokeh.io import output_notebook
from bokeh.palettes import Category20
from bokeh.transform import factor_cmap
output_notebook()

conn = sqlite3.connect('../res/xgb_aucpr_results.db')

def get_tabname():
    # returns all tablenames
    res = conn.execute("SELECT name FROM sqlite_master WHERE type='table';")
    return [name[0] for name in res]

def drop_table(name):
    # function for droping tables from db
    indb = get_tabname()
    if 'xgb_mathelier_results_{}'.format(name) in indb:
        conn.execute('DROP TABLE xgb_mathelier_results_{}'.format(name))
        print('xgb_mathelier_results_{} deleted'.format(name))
    else:
        print('xgb_mathelier_results_{} is not in the db'.format(name))

def get_data_baseline(encoding=False):
    df = pd.read_sql("SELECT name, test_aucpr_mean  FROM xgb_mathelier_results_baseline WHERE encode == '{}'".format(encoding),conn)
    if not encoding:
        df.rename(columns={'test_aucpr_mean':'aucpr_pssm'},inplace=True)
    else:
        df.rename(columns={'test_aucpr_mean':'aucpr_cg_baseline'},inplace=True)
    return df

def get_data(encoding):
    df = pd.read_sql("SELECT name, test_aucpr_mean  FROM xgb_mathelier_results_{}".format(encoding),conn)
    df.rename(columns={'test_aucpr_mean':f'aucpr_{encoding}'},inplace=True)
    return df

def eval_data(df1,df2):
    # df1 should be the baseline
    df_final = pd.merge(df1,df2,on='name')
    cols = df_final.columns
    df_final.plot_bokeh(cols[1],cols[2],kind='scatter')
    print('Datapoints above 0.5: {}'.format(sum(df_final['test_aucpr_mean_y'] >= df_final['test_aucpr_mean_x'])))

def plot_compare(df1,df2):
    tmp = pd.merge(df1,df2,on='name')
    dfl = pd.read_csv('../data/used_TFs_clean_len.csv',dtype={'ENCODE data set':str,'JASPAR2014 matrix identifier':str,'pssm_length':str})
    dfl.rename(columns={'ENCODE data set':'name'},inplace=True)
    tmp2 = pd.merge(tmp,dfl,on='name')
    cols = tmp2.columns
    color_map = factor_cmap('pssm_length', palette=Category20[16], factors=sorted(tmp2.pssm_length.unique()))
    line = np.linspace(0.5,1)
    p = figure(title='Compare mean aucpr',background_fill_color="#fafafa",\
               plot_width=800,plot_height=500, tools='hover,pan,wheel_zoom,box_zoom,reset',\
               tooltips='@name: (@pssm_length)',x_axis_label = f'{cols[1]}',y_axis_label = f'{cols[2]}' )
    p.scatter(cols[1],cols[2], source=tmp2,fill_color=color_map, alpha=0.5, size=10,legend_field='pssm_length')
    p.line(line, line, color="red", line_dash='dashed')
    p.legend.location = 'bottom_right'
    show(p)
    print(f'Datapoints {cols[2]}>={cols[1]}  above 0.5: {sum(tmp2[cols[2]]>=tmp[cols[1]])} of {tmp2.shape[0]}')


def compare_all(dfx):
    lst = ['min_max_MGW','cg_all','intras','inters','zC','zW','mgw','mgw_tilt','mgw_rise',\
       'mgw_strech','mgw_stager','mgw_slide','mgw_shift','mgw_shear','mgw_open','dnashape',\
      'dnashape_small']
    for name in lst:
        print(name)
        df = get_data(name)
        plot_compare(dfx,df)

    return

def plot_featimp(experiment,name,encode,pssm_flag=False):
    # Example call
    # plot_featimp('results_cg_all','wgEncodeAwgTfbsSydhHelas3E2f4UniPk','True_cg_all')
    df = pd.read_csv('../data/results/{0}/features_importance/{1}_{2}.csv'.format(experiment,name,encode))
    df.drop(['Unnamed: 0'],axis=1,inplace=True)
    if pssm_flag:
        df.drop(['pssm'],axis=1,inplace=True)  
    feat_mean = df.mean().sort_values(ascending=True).plot_bokeh(kind='barh',show_figure=False)
    pandas_bokeh.plot_grid([[feat_mean]], plot_width=600, plot_height=800)

def plot_plotly(df1,df2):
    import plotly.graph_objects as go
    df_final = pd.merge(df1,df2,on='name')
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=df_final['test_aucpr_mean_x'],
                                y=df_final['test_aucpr_mean_y'],
                                mode='markers',
                                marker = dict(colorscale='jet',size=10),
                                text=df_final['name'],
                                showlegend=True
                               )) # hover text goes here
    p = np.linspace(0.45,1)
    fig.add_trace(go.Scatter(x=p,y=p,line=dict(color='red',dash='dash'),showlegend=False))

    fig.update_layout(title='AUCPR df1 vs. AUCPR df2',xaxis=dict(title='AUCPR df1',range=[0.5,1.01]),yaxis=dict(title='AUCPR df2',range=[0.5,1.01]),width=750,height=750,showlegend=False)
    fig.show()
