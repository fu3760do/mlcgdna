#!/bin/python

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


def scatter_hist(x, y, ax, ax_histx, ax_histy):
    # no labels
    ax_histx.tick_params(axis="x", labelbottom=False)
    ax_histy.tick_params(axis="y", labelleft=False)

    # the scatter plot:
    ax.scatter(x, y)

    # now determine nice limits by hand:
    binwidth = 0.025
    xymax = max(np.max(np.abs(x)), np.max(np.abs(y)))
    lim = (int(xymax / binwidth) + 1) * binwidth

    bins = np.arange(0, lim + binwidth, binwidth)
    ax_histx.hist(x, bins=bins)
    ax_histy.hist(y, bins=bins, orientation='horizontal')


def plot_data(name):
    # path = ../data/random_data/cutout
    data = pd.read_csv(f'../data/test/{name}')
    # corr = data.loc[:,data.columns[1:-1]].corr()
    filename = name[:-4]

    # fig1, axs = plt.subplots(1,1,figsize=(20,20))
    # fig1.suptitle("Coorelation")
    # sns.heatmap(corr,cmap="RdBu",center=0)
    # fig1.savefig(f'../res/corr/corr_heatmap_{filename}.jpg')

    # fig2, axs2 = plt.subplots(1,1,figsize=(20,20))
    # fig2.suptitle("Heatmap over the data")
    # sns.heatmap(data.loc[:,data.columns[1:-1]],cmap="RdBu",center=0)
    # fig2.savefig(f'../res/corr/data_heatmap_{filename}.jpg')

    # fig3, axs3 = plt.subplots(2,1,figsize=(20,20))
    # fig3.suptitle("Compare histogramm of PSSM and KL",fontsize=16)
    # axs3[0].hist(data['pssm'],bins=100)
    # axs3[0].set_title('Histogram over PSSM scores',fontsize=12)
    # axs3[1].hist(data['kl_sym'],bins=100)
    # axs3[1].set_title('Histogram over KL values',fontsize=12)
    # fig3.savefig(f'../res/corr/hist_kl_pssm_{filename}.jpg')

    # fig4, axs4= plt.subplots(2,1,figsize=(20,20))
    # fig4.suptitle("Compare correlation of PSSM and KL",fontsize=16)
    # axs4[0].plot(corr.loc[:,'pssm'])
    # axs4[0].set_title('Correlation of PSSM score',fontsize=12)
    # axs4[1].plot(corr.loc[:,'kl_sym'])
    # axs4[1].set_title('Correlation over KL score',fontsize=12)
    # fig4.savefig(f'../res/corr/kl_pssm_{filename}.jpg')

    # fig5, axs5 = plt.subplots(1, 1, figsize=(20, 20))
    # fig5.suptitle(f"Scatter Plot of PSSM and KL {filename}")
    # axs5.scatter(data['pssm'], data['kl_sym'])
    # axs5.set_ylabel('kl', fontsize=16)
    # axs5.set_xlabel('pssm', fontsize=16)
    # fig5.savefig(f'../res/results_pssm_kl_random/scatter_kl_pssm_{filename}.jpg')

    if 'mah' in data.columns:
        # definitions for the axes
        left, width = 0.1, 0.65
        bottom, height = 0.1, 0.65
        spacing = 0.005

        rect_scatter = [left, bottom, width, height]
        rect_histx = [left, bottom + height + spacing, width, 0.2]
        rect_histy = [left + width + spacing, bottom, 0.2, height]

        # start with a square Figure
        fig = plt.figure(figsize=(8, 8))
        fig.suptitle(f"Scatter Plot of PSSM and Mahalanobis {filename}")

        ax = fig.add_axes(rect_scatter)
        ax_histx = fig.add_axes(rect_histx, sharex=ax)
        ax_histy = fig.add_axes(rect_histy, sharey=ax)
        ax.set_ylabel('mahalanobis', fontsize=16)
        ax.set_xlabel('pssm', fontsize=16)

        # use the previously defined function
        scatter_hist(data['pssm'], data['mah']/max(data['mah']), ax, ax_histx, ax_histy)

        fig.savefig(f'../data/test/scatter_mahalobis_pssm_{filename}.jpg')


if __name__ == '__main__':
    files = os.listdir('../data/test')
    for file in files:
        if file.startswith('data_'):
            plot_data(file)
