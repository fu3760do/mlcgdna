#!/bin/python

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

def plot_pssm_hist(file):
    names = ['_k2','_k3','_w2','_w3','_g','_c']
    fig, axs = plt.subplots(len(names),1,figsize=(20,20))
    for i,name in enumerate(names):
        df = pd.read_csv(file+name+'.csv')
        axs[i].hist( df[df['Class']==1]['pssm'], bins=50, alpha = .5)
        axs[i].hist( df[df['Class']==0]['pssm'], bins=50, alpha = .5)
        axs[i].legend(['Class 1', 'Class 0'])
        axs[i].set_title('biasaway'+ name)
    fig.suptitle('Compare PSSM for {}'.format(file))
    fig.savefig('../plots/PSSM_hist_{}.jpg'.format(file))

if __name__== '__main__':
    df = pd.read_csv('../../data/test_biasaway.csv')
    for name in df['ENCODE data set']:
        plot_pssm_hist(name)
