import json
import os
import sqlite3
import argparse

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-n',default = None, help = 'name of the table which is going to created')
parser.add_argument('-p',default = None, help = 'path of the folder')
parser.add_argument('-f',default = None , help = 'folder')
args = parser.parse_args()


def create_db(tablename):
    conn = sqlite3.connect('../res/xgb_aucpr_results.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS {}
                (name TEXT,
                 encode TEXT,
                 train_aucpr_0 REAL,
                 test_aucpr_0 REAL,
                 train_aucpr_1 REAL,
                 test_aucpr_1 REAL,
                 train_aucpr_2 REAL,
                 test_aucpr_2 REAL,
                 train_aucpr_3 REAL,
                 test_aucpr_3 REAL,
                 train_aucpr_4 REAL,
                 test_aucpr_4 REAL,
                 train_aucpr_5 REAL,
                 test_aucpr_5 REAL,
                 train_aucpr_6 REAL,
                 test_aucpr_6 REAL,
                 train_aucpr_7 REAL,
                 test_aucpr_7 REAL,
                 train_aucpr_8 REAL,
                 test_aucpr_8 REAL,
                 train_aucpr_9 REAL,
                 test_aucpr_9 REAL,
                 train_aucpr_mean REAL,
                 test_aucpr_mean REAL,
                 filename TEXT
                )
             '''.format(tablename))
    conn.commit()
    conn.close()
    return

def content_in_db(path,folder,tabledb):
    conn = sqlite3.connect('../res/xgb_aucpr_results.db')
    c = conn.cursor()
    files = os.listdir(os.path.join(path,folder))
    for file in files:
        with open(os.path.join(path,os.path.join(folder,file)),'r') as f:
            data = json.load(f)
            c.execute('''SELECT filename FROM {0}'''.format(tabledb))
            dbfiles = [item[0] for item in c.fetchall()]
            if file.split('.')[0] not in dbfiles:
                c.execute('''INSERT INTO {} VALUES
                          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''.format(tabledb),\
                (data['name'],data['encode'],data['train_aucpr_0'],data['test_aucpr_0'],data['train_aucpr_1'],data['test_aucpr_1'],data['train_aucpr_2'],data['test_aucpr_2'],data['train_aucpr_3'],data['test_aucpr_3'],data['train_aucpr_4'],data['test_aucpr_4'],data['train_aucpr_5'],data['test_aucpr_5'],data['train_aucpr_6'],data['test_aucpr_6'],data['train_aucpr_7'],data['test_aucpr_7'],data['train_aucpr_8'],data['test_aucpr_8'],data['train_aucpr_9'],data['test_aucpr_9'],data['mean_train_aucpr'],data['mean_test_aucpr'],file.split('.')[0]))
            else:
                continue
        conn.commit()
    conn.close()
    return

if __name__ == '__main__':
    if '+' in args.n:
        tmp = args.n.split('+')
        tabname = tmp[0]+'_'+tmp[1]
    else:
        tabname = args.n
    create_db(tabname)
    content_in_db(args.p,args.f,tabname)
