#!/bin/bash

input="./data/used_TFs_clean.csv"
line="$(awk 'NR=='"($1)" $input)"
file="$(echo $line | cut -d',' -f1)"
motif="$(echo $line | cut -d',' -f2)"
fname=./data/mat_data/foreground/fore_$file.fa
dpath=hgdownload.cse.ucsc.edu/goldenpath/hg19/encodeDCC/wgEncodeAwgTfbsUniform/
echo $dpath$file
wget $dpath$file.narrowPeak.gz -P ./data/
gunzip -c ./data/$file.narrowPeak.gz > ./data/$file
bedtools getfasta -fi ./data/hg19.fa -bed ./data/$file -fo $fname
biasaway k -f $fname > ./data/mat_data/background/back_$file.fa
head -n 100 $fname > ./data/mat_data/foreground/fore_$file'_debug'.fa
head -n 100 ./data/mat_data/background/back_$file.fa > ./data/mat_data/background/back_$file'_debug'.fa
python mathelier_cgDNA.py -f $file -m $motif -e 1 

